-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 01 2023 г., 06:37
-- Версия сервера: 10.4.21-MariaDB
-- Версия PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `addresses`
--

CREATE TABLE `addresses` (
  `address_id` int(10) UNSIGNED NOT NULL,
  `address_name` text NOT NULL,
  `address_user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `addresses`
--

INSERT INTO `addresses` (`address_id`, `address_name`, `address_user_id`) VALUES
(1, '191144, Санкт-Петербург, Советская 8-я, 44, кв.5', 2),
(2, '195009, Санкт-Петербург, Боткинская, 1, кв.23', 3),
(3, '194064, Санкт-Петербург, Тихорецкий проспект, 5 к2, кв.54', 1),
(4, '198262, Санкт-Петербург, Дачный проспект, 5 к3, кв.123', 4),
(5, '192131, Санкт-Петербург, Бабушкина, 52, кв.10', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE `authors` (
  `author_id` int(10) UNSIGNED NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `author_is_deleted` tinyint(1) UNSIGNED NOT NULL COMMENT 'Флаг для удаления'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`author_id`, `author_name`, `author_is_deleted`) VALUES
(1, 'Вейер, Энди', 0),
(2, 'Гейман, Нил', 0),
(3, 'Пратчетт, Терри', 0),
(4, 'Кинг, Стивен', 0),
(5, 'Мартин, Джордж Р.Р.', 0),
(6, 'Ролинг Дж.К.', 0),
(7, 'Харари Ю.Н.', 0),
(8, 'Голсуорси, Джон', 0),
(9, 'Пропп В.', 0),
(10, 'Толстой, Алексей Константинович', 1),
(11, 'Толстой, Лев Николаевич', 0),
(13, '111', 1),
(14, 'Улицкая, Людмила Евгеньевна', 1),
(15, 'Пушкин', 0),
(16, '', 1),
(23, 'Чехов, Антон Павлович', 0),
(24, 'Толкин, Джон Р.Р.', 0),
(25, 'Рэнд, Айн', 0),
(26, 'Кристи, Агата', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `book_id` int(10) UNSIGNED NOT NULL,
  `book_name` varchar(255) DEFAULT NULL,
  `book_isbn` char(13) DEFAULT NULL,
  `book_price` smallint(5) UNSIGNED DEFAULT NULL,
  `book_weight` smallint(5) UNSIGNED DEFAULT NULL,
  `book_publisher_id` int(10) UNSIGNED DEFAULT NULL,
  `book_count` smallint(5) UNSIGNED DEFAULT NULL,
  `book_genre_id` int(10) UNSIGNED DEFAULT NULL,
  `book_avarage_mark` float(3,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`book_id`, `book_name`, `book_isbn`, `book_price`, `book_weight`, `book_publisher_id`, `book_count`, `book_genre_id`, `book_avarage_mark`) VALUES
(1, 'Марсианин', '9785170844043', 576, 410, 1, 10, 1, 4.50),
(2, 'Американские боги', '9785170967667', 403, 318, 1, 28, 1, 0.00),
(3, 'Пятый элефант. Ночная стража', '9785699918973', 696, 750, 2, 8, 1, 0.00),
(4, 'Ветер сквозь замочную скважину: из цикла \"Темная Башня\"', '9785170770670', 472, 390, 1, 13, 1, 0.00),
(5, 'Буря мечей. Том I', '9785171152307', 1668, 1166, 1, 37, 1, 0.00),
(15, 'Морфология волшебной сказки ; Истоические корни волшебной сказки ; Русский героический эпос', '9785389194335', 1126, 1153, 6, 45, 4, 0.00),
(16, 'Сага о Форсайтах: Т. 1', '9785699275113', 590, 747, 5, 38, 1, 0.00),
(17, 'Homo Deus. Краткая история будущего', '9785906837776', 975, 446, 3, 63, 4, 0.00),
(18, 'Гарри Поттер и Дары Смерти', '9785353029076', 990, 488, 4, 70, 1, 0.00),
(23, 'Война И Мир', '5234567890124', NULL, NULL, 5, NULL, NULL, 0.00),
(24, 'Анна Каренина', '', NULL, NULL, 5, NULL, NULL, 0.00),
(25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00),
(26, 'Поваренная книга', '1234567890125', NULL, NULL, 7, NULL, NULL, 0.00),
(27, 'Поваренная книга нянюшки Ягг', '1234567890124', NULL, NULL, 1, NULL, NULL, 0.00),
(28, 'Вишневый сад', '1234567890123', NULL, NULL, 5, NULL, NULL, 0.00),
(29, 'Убийство в &quot;Восточном экспрессе&quot;', '9785041012779', NULL, NULL, 5, NULL, NULL, 0.00);

-- --------------------------------------------------------

--
-- Структура таблицы `books_authors`
--

CREATE TABLE `books_authors` (
  `book_author_id` int(10) UNSIGNED NOT NULL,
  `book_author_author_id` int(10) UNSIGNED NOT NULL,
  `book_author_book_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books_authors`
--

INSERT INTO `books_authors` (`book_author_id`, `book_author_author_id`, `book_author_book_id`) VALUES
(10, 1, 1),
(11, 2, 2),
(12, 3, 3),
(13, 4, 4),
(14, 5, 5),
(15, 6, 18),
(16, 7, 17),
(17, 8, 16),
(18, 9, 15),
(25, 11, 24),
(26, 11, 23),
(28, 3, 27),
(29, 23, 28),
(30, 26, 29),
(43, 15, 26);

-- --------------------------------------------------------

--
-- Структура таблицы `carts`
--

CREATE TABLE `carts` (
  `cart_id` int(10) UNSIGNED NOT NULL,
  `cart_book_id` int(10) UNSIGNED NOT NULL,
  `cart_book_count` tinyint(2) UNSIGNED NOT NULL,
  `cart_order_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `carts`
--

INSERT INTO `carts` (`cart_id`, `cart_book_id`, `cart_book_count`, `cart_order_id`) VALUES
(1, 1, 1, 2),
(2, 1, 1, 3),
(3, 2, 1, 5),
(4, 5, 2, 1),
(5, 3, 2, 3),
(7, 15, 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `connects`
--

CREATE TABLE `connects` (
  `connect_id` int(10) UNSIGNED NOT NULL,
  `connect_user_id` int(10) UNSIGNED NOT NULL,
  `connect_token` char(32) NOT NULL,
  `connect_token_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `connects`
--

INSERT INTO `connects` (`connect_id`, `connect_user_id`, `connect_token`, `connect_token_time`) VALUES
(2, 9, '8FKE1PACF54PFECFFHL93L15GLBCCJC1', '2021-12-14 22:56:51'),
(3, 9, 'MDPDL02J7PD501ED93PDB8B8G8P6KIO4', '2021-11-13 11:44:49'),
(4, 9, 'GHP4J15HJD8IO8LJO4B7HD7GG153EB92', '2021-11-13 11:45:06'),
(5, 9, '89D613OAGAI32LFG7GDIG04ADGE5LHL2', '2021-11-13 11:47:14'),
(6, 9, 'MA38IKJ5JDF876KIPMGCJILINMDML4MD', '2021-11-13 12:46:12'),
(7, 9, 'H5P9381B7JIFA5729P6BKH4HO136OCN6', '2021-11-13 12:49:58'),
(8, 9, 'FJ2IL7G7BKFHBKCFJ14EFBF68EP5G6IM', '2021-11-13 13:39:32'),
(9, 9, '9EIAN4PBFJCML0H1E66FM7HME83J0MO6', '2021-11-15 20:41:53'),
(14, 14, '97LL634AKH2KBE672N827H21BDD5F5NC', '2021-12-13 19:49:23'),
(16, 16, 'H1GC483LE2M78CBM6I9K8BPP0K4K8EKF', '2021-12-13 19:56:58'),
(17, 17, 'E0GA1BKP4D04D1E1I5GLI44477J7L980', '2021-12-13 20:13:04'),
(18, 17, 'OG8PKNJO87N992B2OGC60PKL4P8GOOA3', '2021-12-13 20:13:22'),
(20, 17, 'OLC4E7H54CFLHL86DKNEBJ3469J54EF4', '2021-12-14 18:06:44'),
(28, 9, 'MB0D86G0A1AF3LFP4BHG4GM93HNAMGJ1', '2021-12-21 20:33:37');

-- --------------------------------------------------------

--
-- Структура таблицы `genders`
--

CREATE TABLE `genders` (
  `gender_id` tinyint(2) UNSIGNED NOT NULL,
  `gender_name` varchar(255) DEFAULT NULL,
  `gender_short_name` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genders`
--

INSERT INTO `genders` (`gender_id`, `gender_name`, `gender_short_name`) VALUES
(1, 'Мужской', 'М'),
(2, 'Женский', 'Ж');

-- --------------------------------------------------------

--
-- Структура таблицы `genres`
--

CREATE TABLE `genres` (
  `genre_id` int(10) UNSIGNED NOT NULL,
  `genre_name` varchar(255) NOT NULL,
  `genre_is_deleted` tinyint(1) UNSIGNED NOT NULL COMMENT 'Флаг для удаления'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genres`
--

INSERT INTO `genres` (`genre_id`, `genre_name`, `genre_is_deleted`) VALUES
(1, 'Художественная литература', 0),
(2, 'Учебная, методическая литература и словари', 0),
(3, 'Периодические издания', 0),
(4, 'Специальная литература', 0),
(5, 'Книги для детей', 0),
(6, 'Комиксы. Манга. Артбуки', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `marks`
--

CREATE TABLE `marks` (
  `mark_id` int(10) UNSIGNED NOT NULL,
  `mark_value` tinyint(1) UNSIGNED NOT NULL,
  `mark_book_id` int(10) UNSIGNED NOT NULL,
  `mark_user_id` int(10) UNSIGNED NOT NULL,
  `mark_comment` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `marks`
--

INSERT INTO `marks` (`mark_id`, `mark_value`, `mark_book_id`, `mark_user_id`, `mark_comment`) VALUES
(1, 5, 1, 1, NULL),
(2, 5, 2, 3, NULL),
(3, 4, 1, 2, NULL),
(4, 3, 5, 4, NULL),
(5, 4, 3, 2, NULL),
(6, 4, 1, 3, NULL),
(7, 5, 1, 6, NULL),
(8, 5, 1, 17, NULL),
(9, 4, 1, 9, NULL);

--
-- Триггеры `marks`
--
DELIMITER $$
CREATE TRIGGER `update_book_avarage_mark_on_new_mark` AFTER INSERT ON `marks` FOR EACH ROW UPDATE `books`
SET `book_avarage_mark` = (
    SELECT AVG(`mark_value`)
    FROM `marks`
    WHERE `mark_book_id` = NEW.`mark_book_id`
)
WHERE `book_id` = NEW.`mark_book_id`
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_address_id` int(10) UNSIGNED DEFAULT NULL,
  `order_status_id` tinyint(2) UNSIGNED DEFAULT NULL,
  `order_delivery_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`order_id`, `order_address_id`, `order_status_id`, `order_delivery_time`) VALUES
(1, 4, 3, '0000-00-00'),
(2, 1, 4, '0000-00-00'),
(3, 2, 4, '0000-00-00'),
(4, 5, 2, '0000-00-00'),
(5, 3, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Структура таблицы `publishers`
--

CREATE TABLE `publishers` (
  `publisher_id` int(10) UNSIGNED NOT NULL,
  `publisher_name` varchar(255) NOT NULL,
  `publisher_is_deleted` tinyint(1) UNSIGNED NOT NULL COMMENT 'Флаг для удаления'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publishers`
--

INSERT INTO `publishers` (`publisher_id`, `publisher_name`, `publisher_is_deleted`) VALUES
(1, 'Издательство АСТ', 0),
(2, 'Издательство \"Э\"', 0),
(3, 'Синдбад', 0),
(4, 'ЗАО \"РОСМЭН-ПРЕСС\"', 0),
(5, 'Эксмо', 0),
(6, 'Азбука', 0),
(7, 'Манн, Иванов и Фербер', 0),
(8, 'МИФ', 1),
(9, 'Издательство &quot;ПИТЕР&quot;', 1),
(10, 'Новое', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE `statuses` (
  `status_id` tinyint(2) UNSIGNED NOT NULL,
  `status_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `statuses`
--

INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
(1, 'В обработке'),
(4, 'Доставлен'),
(3, 'Отправлен'),
(2, 'Сборка');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_login` varchar(255) NOT NULL,
  `user_password` char(32) NOT NULL,
  `user_phone` varchar(30) DEFAULT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_dob` date DEFAULT NULL,
  `user_gender_id` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_login`, `user_password`, `user_phone`, `user_email`, `user_dob`, `user_gender_id`) VALUES
(1, 'admin', 'admin', '123', '79211111111', 'admin@test.com', '2001-10-30', 1),
(2, 'user1', 'user1', 'qwerty', '79111111111', 'user1@test.com', '2001-01-01', 2),
(3, 'user2', 'user2', 'qweasd', '79110000001', 'user2@test.com', '1991-11-11', 1),
(4, 'user3', 'user3', '11111', '79044440000', 'user3@test.com', '1999-05-15', 1),
(5, 'user4', 'user4', '222222', '79811234567', 'user4@test.com', '1985-08-28', 2),
(6, NULL, 'user-user', '55555', NULL, 'abc@test.ru', NULL, NULL),
(7, NULL, 'qwe', '222', NULL, 'qwe@test.ru', NULL, NULL),
(9, NULL, 'asd', '81dc9bdb52d04dc20036dbd8313ed055', NULL, 'asd@test.ru', NULL, NULL),
(14, NULL, 'test', '81dc9bdb52d04dc20036dbd8313ed055', NULL, 'test@test.ru', NULL, NULL),
(16, NULL, 'test2', 'c5fe25896e49ddfe996db7508cf00534', NULL, 'test2@test.ru', NULL, NULL),
(17, NULL, '111', 'b59c67bf196a4758191e42f76670ceba', NULL, 'abc1@test.ru', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `address_user_id` (`address_user_id`);

--
-- Индексы таблицы `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`author_id`),
  ADD UNIQUE KEY `author_name` (`author_name`);

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`),
  ADD UNIQUE KEY `book_isbn` (`book_isbn`),
  ADD KEY `book_publisher_id` (`book_publisher_id`),
  ADD KEY `book_genre_id` (`book_genre_id`);

--
-- Индексы таблицы `books_authors`
--
ALTER TABLE `books_authors`
  ADD PRIMARY KEY (`book_author_id`),
  ADD KEY `book_author_author_id` (`book_author_author_id`),
  ADD KEY `book_author_book_id` (`book_author_book_id`);

--
-- Индексы таблицы `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_book_id` (`cart_book_id`),
  ADD KEY `cart_order_id` (`cart_order_id`);

--
-- Индексы таблицы `connects`
--
ALTER TABLE `connects`
  ADD PRIMARY KEY (`connect_id`),
  ADD KEY `connect_user_id` (`connect_user_id`);

--
-- Индексы таблицы `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`gender_id`),
  ADD UNIQUE KEY `gender_name` (`gender_name`),
  ADD UNIQUE KEY `gender_short_name` (`gender_short_name`);

--
-- Индексы таблицы `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`genre_id`),
  ADD UNIQUE KEY `genre_name` (`genre_name`);

--
-- Индексы таблицы `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`mark_id`),
  ADD KEY `mark_book_id` (`mark_book_id`),
  ADD KEY `mark_user_id` (`mark_user_id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_address_id` (`order_address_id`),
  ADD KEY `order_status_id` (`order_status_id`);

--
-- Индексы таблицы `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`publisher_id`),
  ADD UNIQUE KEY `publisher_name` (`publisher_name`);

--
-- Индексы таблицы `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`status_id`),
  ADD UNIQUE KEY `status_name` (`status_name`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_login` (`user_login`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD UNIQUE KEY `user_phone` (`user_phone`),
  ADD KEY `user_gender_id` (`user_gender_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `addresses`
--
ALTER TABLE `addresses`
  MODIFY `address_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `authors`
--
ALTER TABLE `authors`
  MODIFY `author_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблицы `books_authors`
--
ALTER TABLE `books_authors`
  MODIFY `book_author_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT для таблицы `carts`
--
ALTER TABLE `carts`
  MODIFY `cart_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `connects`
--
ALTER TABLE `connects`
  MODIFY `connect_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `genders`
--
ALTER TABLE `genders`
  MODIFY `gender_id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `genres`
--
ALTER TABLE `genres`
  MODIFY `genre_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `marks`
--
ALTER TABLE `marks`
  MODIFY `mark_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `publishers`
--
ALTER TABLE `publishers`
  MODIFY `publisher_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `statuses`
--
ALTER TABLE `statuses`
  MODIFY `status_id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`address_user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`book_publisher_id`) REFERENCES `publishers` (`publisher_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `books_ibfk_2` FOREIGN KEY (`book_genre_id`) REFERENCES `genres` (`genre_id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `books_authors`
--
ALTER TABLE `books_authors`
  ADD CONSTRAINT `books_authors_ibfk_1` FOREIGN KEY (`book_author_author_id`) REFERENCES `authors` (`author_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_authors_ibfk_2` FOREIGN KEY (`book_author_book_id`) REFERENCES `books` (`book_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`cart_book_id`) REFERENCES `books` (`book_id`),
  ADD CONSTRAINT `carts_ibfk_2` FOREIGN KEY (`cart_order_id`) REFERENCES `orders` (`order_id`);

--
-- Ограничения внешнего ключа таблицы `connects`
--
ALTER TABLE `connects`
  ADD CONSTRAINT `connects_ibfk_1` FOREIGN KEY (`connect_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `marks`
--
ALTER TABLE `marks`
  ADD CONSTRAINT `marks_ibfk_1` FOREIGN KEY (`mark_book_id`) REFERENCES `books` (`book_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `marks_ibfk_2` FOREIGN KEY (`mark_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`order_address_id`) REFERENCES `addresses` (`address_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`order_status_id`) REFERENCES `statuses` (`status_id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_gender_id`) REFERENCES `genders` (`gender_id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
